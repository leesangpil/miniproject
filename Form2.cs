﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Miniproject
{
    public partial class Form2 : Form
    {
        public Image image = null;
        public static int HumanImageCount = 0;

        public Form2()
        {
            InitializeComponent();
        }

        private void 사진불러오기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.DefaultExt = "jpg";
            openFile.Filter = "image file (*.jpg)|*.jpg|All files(*.*)|*.*";
            openFile.ShowDialog();

            if (openFile.FileName.Length > 0)
            {
                image = Image.FromFile(openFile.FileName);

                pictureBox1.Image = image;
            }
        }

        private void button1_Click(object sender, EventArgs e)          // 확인 버튼
        {
            CreateBitmap();
        }

        private void button2_Click(object sender, EventArgs e)          // 취소 버튼
        {
            this.Close();
        }

        void CreateBitmap()
        {
            int Width = 140;
            int Height = 180;
            bool select_sex = false;

            if (image == null)                      // 사진이 없을 때
            {
                MessageBox.Show("이미지를 삽입하세요.", "이미지 삽입 에러");
                this.Close();
            }
            else
            {
                Bitmap input = new Bitmap(image);       // 140 * 180 이미지
                Bitmap output = new Bitmap(432, 240);

                for (int i = 0; i < 432; i++)
                    for (int j = 0; j < 240; j++)
                        output.SetPixel(i, j, Color.White);     // 초기화 (흰색)

                for (int i = 50; i < Width + 50; i++)           // 해당위치에 그림 삽입하기 (50, 30)
                    for (int j = 20; j < Height + 20; j++)
                        output.SetPixel(i, j, input.GetPixel(i - 50, j - 20));

                // output이미지에 글씨 쓰기
                Graphics g = Graphics.FromImage(output);                          // 그래팩 클래스 생성 (이미지로부터)

                Brush blackBrush = new SolidBrush(Color.Black);                   // 폰트 색
                FontFamily familyName = new FontFamily("굴림");                   // 폰트생성
                System.Drawing.Font myFont = new System.Drawing.Font(familyName, 14, FontStyle.Regular, GraphicsUnit.Pixel);

                Point namePoint = new Point(220, 20);                             // 글자 시작위치 (이름)
                Point agePoint = new Point(220, 60);                              // 글자 시작위치 (나이)
                Point sexPoint = new Point(220, 100);                             // 글자 시작위치 (성별)
                Point charPoint = new Point(220, 140);                            // 글자 시작위치 (특징)

                g.DrawString("이름 : ", myFont, blackBrush, namePoint);           // 글자 쓰기
                g.DrawString("나이 : ", myFont, blackBrush, agePoint);            // 글자 쓰기
                g.DrawString("성별 : ", myFont, blackBrush, sexPoint);            // 글자 쓰기
                g.DrawString("특징 : ", myFont, blackBrush, charPoint);           // 글자 쓰기

                Point input_namePoint = new Point(270, 20);                       // 입력글자 시작위치 (이름)
                Point input_agePoint = new Point(270, 60);                        // 입력글자 시작위치 (나이)
                Point input_sexPoint = new Point(270, 100);                       // 입력글자 시작위치 (성별)

                Point input_charPoint1 = new Point(270, 140);                     // 입력글자 시작위치 (특징)
                Point input_charPoint2 = new Point(270, 160);
                Point input_charPoint3 = new Point(270, 180);
                Point input_charPoint4 = new Point(270, 200);

                String input_name = textBox1.Text;
                String input_age = textBox2.Text;
                String input_sex = " ";

                if (radioButton1.Checked == true && radioButton2.Checked == false)
                {
                    select_sex = true;
                    input_sex = "남성";
                }
                else if (radioButton1.Checked == false && radioButton2.Checked == true)
                {
                    select_sex = true;
                    input_sex = "여성";
                }
                else
                {
                    select_sex = false;
                    MessageBox.Show("성별을 선택하세요.", "성별선택 에러");
                }

                String input_char = textBox3.Text;

                int split_count = 0;

                if (input_char.Length > 10)
                {
                    split_count++;
                    if (input_char.Length > 20)
                    {
                        split_count++;
                        if (input_char.Length > 30)
                        {
                            split_count++;
                            if (input_char.Length > 40)
                            {
                                split_count++;
                            }
                        }
                    }
                }

                if (split_count == 0)           // 글자수가 0 ~ 10 사이 일 때,
                {
                    // 나누지 않음
                    g.DrawString(input_char, myFont, blackBrush, input_charPoint1);            // 입력글자 쓰기
                }
                else if (split_count == 1)      // 글자수가 10 ~ 20 사이 일 때,
                {
                    String sub_str1 = input_char.Substring(0, 10);
                    String sub_str2 = input_char.Substring(10, input_char.Length - 10);
                    g.DrawString(sub_str1, myFont, blackBrush, input_charPoint1);            // 입력글자 쓰기
                    g.DrawString(sub_str2, myFont, blackBrush, input_charPoint2);            // 입력글자 쓰기
                }
                else if (split_count == 2)      // 글자수가 20 ~ 30 사이 일 때,
                {
                    String sub_str1 = input_char.Substring(0, 10);
                    String sub_str2 = input_char.Substring(10, 10);
                    String sub_str3 = input_char.Substring(20, input_char.Length - 20);
                    g.DrawString(sub_str1, myFont, blackBrush, input_charPoint1);            // 입력글자 쓰기
                    g.DrawString(sub_str2, myFont, blackBrush, input_charPoint2);            // 입력글자 쓰기
                    g.DrawString(sub_str3, myFont, blackBrush, input_charPoint3);            // 입력글자 쓰기
                }
                else if (split_count == 3)      // 글자수가 30 ~ 40 사이 일 때,
                {
                    String sub_str1 = input_char.Substring(0, 10);
                    String sub_str2 = input_char.Substring(10, 10);
                    String sub_str3 = input_char.Substring(20, 10);
                    String sub_str4 = input_char.Substring(30, input_char.Length - 30);
                    g.DrawString(sub_str1, myFont, blackBrush, input_charPoint1);            // 입력글자 쓰기
                    g.DrawString(sub_str2, myFont, blackBrush, input_charPoint2);            // 입력글자 쓰기
                    g.DrawString(sub_str3, myFont, blackBrush, input_charPoint3);            // 입력글자 쓰기
                    g.DrawString(sub_str4, myFont, blackBrush, input_charPoint4);            // 입력글자 쓰기
                }
                else                            // 글자수가 50자를 초과 했을 때,
                {
                    MessageBox.Show("글자수를 40자 초과하셨습니다.", "입력 초과 에러");
                    select_sex = false;
                    this.Close();
                }

                if (select_sex == true)
                {
                    g.DrawString(input_name, myFont, blackBrush, input_namePoint);             // 입력글자 쓰기
                    g.DrawString(input_age, myFont, blackBrush, input_agePoint);               // 입력글자 쓰기
                    g.DrawString(input_sex, myFont, blackBrush, input_sexPoint);               // 입력글자 쓰기
                    g.DrawString(input_char, myFont, blackBrush, input_charPoint1);            // 입력글자 쓰기

                    g.DrawImage(output, new Point(10, 10));                           // ?? 넣어줘야 비트맵으로 씌여지는 듯
                    Form1.pictureBox2.BackgroundImage = output;

                    String SaveImageName = "C:\\Users\\splee\\Documents\\Visual Studio 2010\\Projects\\Miniproject\\HumanImages\\";
                    SaveImageName = SaveImageName + HumanImageCount + ".jpg";
                    output.Save(SaveImageName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    HumanImageCount++;
                }
            }
        }
    }
}

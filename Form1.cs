﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;

using System.Threading;

namespace Miniproject
{
    public partial class Form1 : Form
    {
        string DispString;
        string tempLength1;
        string tempLength2;

        public static int please = 0;
        int Cnt1 = 0;

        bool ackFlag = false;

        private ThreadStart _ts = null;
        private Thread _t = null;

        public String LoadImageNameOfHuman = "C:\\Users\\splee\\Documents\\Visual Studio 2010\\Projects\\Miniproject\\HumanImages\\";
        public String LoadImageNameOfMap = "C:\\Users\\splee\\Documents\\Visual Studio 2010\\Projects\\Miniproject\\MapImages\\";

        public static int MapImageCount = 0;
        public int totalReceivedLength = 0;

        public bool isOpenPort = false;

        public Form1()
        {
            InitializeComponent();
        }

        // 프로그램 종료
        private void 종료하기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // 파일 불러오기(이미지)
        private void 파일불러오기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.DefaultExt = "jpg";
            openFile.Filter = "image file (*.jpg)|*.jpg|All files(*.*)|*.*";
            openFile.ShowDialog();
            if (openFile.FileName.Length > 0)
            {
                Image image = Image.FromFile(openFile.FileName);
                pictureBox1.Image = image;
            }
        }

        private void 피구조자정보생성ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.Show();
        }

        private void 도면불러오기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.DefaultExt = "jpg";
            openFile.Filter = "image file (*.jpg)|*.jpg|All files(*.*)|*.*";
            openFile.ShowDialog();

            if (openFile.FileName.Length > 0)
            {
                Image image = Image.FromFile(openFile.FileName);
                pictureBox3.Image = image;
            }

            MapImageCount++;
        }

        private void OpenSerialPort()
        {
            try
            {
                isOpenPort = true;
                //serialPort1.PortName = "COM4";      // 무선포트
                //serialPort1.PortName = "COM3";      // 유선포트
                serialPort1.PortName = SerialPort.GetPortNames()[0];

                serialPort1.BaudRate = 115200;
                serialPort1.Parity = Parity.None;
                serialPort1.StopBits = StopBits.One;
                serialPort1.Handshake = Handshake.None;
                //serialPort1.Encoding = Encoding.;
                //serialPort1.Handshake = Handshake.XOnXOff;      // Handshake

                serialPort1.Open();
                serialPort1.ReadTimeout = 200;
                if (serialPort1.IsOpen)
                {
                    DispString = "";
                    this.textBox2.Text = "";
                }
                serialPort1.DataReceived += new SerialDataReceivedEventHandler(serialPort1_DataReceived);

                button3.Enabled = !serialPort1.IsOpen;    // OPEN BUTTON Disable
                button4.Enabled = serialPort1.IsOpen;     // CLOSE BUTTON Enable
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CloseSerialPort()
        {
            isOpenPort = false;
            // serialPort1 이 null 아닐때만 close 처리를 해준다.
            if (serialPort1 != null)
            {
                if (serialPort1.IsOpen)
                    serialPort1.Close();
            }
            button3.Enabled = true;
            button4.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)          // OPEN
        {
            //if (isOpenPort == false)
                OpenSerialPort();
        }

        private void button4_Click(object sender, EventArgs e)          // CLOSE
        {
           // if (isOpenPort == true)
                CloseSerialPort();
        }

        // 메시지 전송
        private void button0_Click(object sender, EventArgs e)
        {
            //if (isOpenPort == false)            // 포트가 닫혀있으면,
                //OpenSerialPort();               // 포트를 열고, 메시지 전송

            if (textBox1.Text.Length == 0)  // 텍스트가 아무것도 입력되지 않았을 때,
            {
                MessageBox.Show("텍스트를 입력해 주세요.", "메시지 전송 에러");
            }
            else                        // 텍스트가 입력 되었을 때,
            {
                try
                {
                    serialPort1.Write("1/");        // text 플래그
                    serialPort1.Write(textBox1.Text);
                }
                catch (System.Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            // 메시지 전송 후 포트를 닫음
            //CloseSerialPort();
        }

        // 도면 전송
        private void button1_Click(object sender, EventArgs e)
        {
            //if (isOpenPort == false)
                //OpenSerialPort();

            try
            {
                MemoryStream ms1 = new MemoryStream();

                String tempName = LoadImageNameOfMap + MapImageCount + ".jpg";

                Image temp1 = Image.FromFile(tempName);

                temp1.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);

                byte[] sendArray1 = new byte[ms1.ToArray().Length];
                sendArray1 = ms1.ToArray();

                serialPort1.Write("2/");    // 도면 플래그
                serialPort1.Write(sendArray1.Length.ToString());    // 도면 바이트 길이

                tempLength1 = sendArray1.Length.ToString();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        // 피 구조자 정보 전송
        private void button2_Click(object sender, EventArgs e)
        {
           // if (isOpenPort == false)
                //OpenSerialPort();
            try
            {
                MemoryStream ms2 = new MemoryStream();

                String tempName = LoadImageNameOfHuman + (Form2.HumanImageCount - 1) + ".jpg";

                //Image temp2 = Image.FromFile("C:\\Users\\splee\\Documents\\1.jpg");
                Image temp2 = Image.FromFile(tempName);

                temp2.Save(ms2, System.Drawing.Imaging.ImageFormat.Jpeg);

                byte[] sendArray2 = new byte[ms2.ToArray().Length];
                sendArray2 = ms2.ToArray();

                serialPort1.Write("3/");            // 피 구조자 플래그
                serialPort1.Write(sendArray2.Length.ToString());     // 피 구조자 정보 길이

                tempLength2 = sendArray2.Length.ToString();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void ThreadFunc()
        {
            Delay(5000);

            if (ackFlag == false)
            {
                MessageBox.Show("time out");
                serialPort1.Write("20101");
                _t.Abort();
            }
        }

        // data received
        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //DispString = serialPort1.ReadExisting();
            try
            {
                byte[] buf = new byte[90];
                serialPort1.Read(buf, 0, 90);
                //serialPort1.Read




                //String check_Str = Encoding.UTF8.GetString(buf);

                //DispString = Convert.ToString(buf);

                //Console.WriteLine(DispString);

                if (DispString == "A")
                {
                    MemoryStream ms3 = new MemoryStream();

                    String tempName = LoadImageNameOfMap + MapImageCount + ".jpg";

                    Image temp3 = Image.FromFile(tempName);
                    //Image temp3 = Image.FromFile("C:\\Users\\splee\\Documents\\section.jpg");

                    temp3.Save(ms3, System.Drawing.Imaging.ImageFormat.Jpeg);

                    byte[] sendArray3 = new byte[ms3.ToArray().Length];
                    sendArray3 = ms3.ToArray();

                    ackFlag = false;

                    _ts = new ThreadStart(ThreadFunc);
                    _t = new Thread(_ts);
                    _t.Start();

                    int buffer_size1 = 90;       // 90
                    int count1 = 0;
                    while (true)
                    {
                        if (sendArray3.Length - count1 * buffer_size1 < buffer_size1)
                        {
                            serialPort1.Write(sendArray3, count1 * buffer_size1, sendArray3.Length - count1 * buffer_size1);
                            count1 = 0;
                            break;
                        }

                        serialPort1.Write(sendArray3, count1 * buffer_size1, buffer_size1);
                        Delay(30);              // 28
                        count1++;
                    }
                }
                else if (DispString == "B")
                {
                    MemoryStream ms4 = new MemoryStream();
                    String tempName = LoadImageNameOfHuman + (Form2.HumanImageCount - 1) + ".jpg";

                    //Image temp4 = Image.FromFile("C:\\Users\\splee\\Documents\\1.jpg");
                    Image temp4 = Image.FromFile(tempName);

                    temp4.Save(ms4, System.Drawing.Imaging.ImageFormat.Jpeg);

                    byte[] sendArray4 = new byte[ms4.ToArray().Length];
                    sendArray4 = ms4.ToArray();

                    ackFlag = false;

                    _ts = new ThreadStart(ThreadFunc);
                    _t = new Thread(_ts);
                    _t.Start();

                    int buffer_size = 90;       // 90

                    int count = 0;

                    while (true)
                    {
                        if (sendArray4.Length - count * buffer_size < buffer_size)
                        {
                            serialPort1.Write(sendArray4, count * buffer_size, sendArray4.Length - count * buffer_size);
                            count = 0;
                            break;
                        }

                        serialPort1.Write(sendArray4, count * buffer_size, buffer_size);
                        Delay(30);              // 28
                        count++;
                    }
                }
                //else if (DispString == "C")
                //{
                //   MessageBox.Show("Received Data");
                //}
                else if (DispString == tempLength1)
                {
                    ackFlag = true;
                    MessageBox.Show("good");
                    //CloseSerialPort();
                }
                else if (DispString == tempLength2)
                {
                    ackFlag = true;
                    MessageBox.Show("good");
                    //CloseSerialPort();
                }
                else
                {
                    //String temp = Encoding.ASCII.GetString(buf);

                    for (int i = 0; i < buf.Length; i++)
                    {
                        if ((char)(buf[i]) != 'c')
                        {
                            Console.Write(buf[i] + " ");
                            Cnt1++;
                        }
                        //if (temp[i] != '\0')
                        //{
                        //    Console.Write(temp[i] + " ");
                        //    Cnt1++;
                        //}
                    }
                    Console.WriteLine(" ");
                    Console.WriteLine("Total Received Data : " + Cnt1);

                    //Console.WriteLine(DispString);

                    //for (int i = 0; i < DispString.Length; i++)
                    //{
                    //    //if ((int)DispString[i] > 120)
                    //    Console.Write((byte)DispString[i] + " ");
                    //}
                    //Console.WriteLine(" ");
                    //Console.WriteLine("data length : " + DispString.Length);


                    //byte[] receivedData = new byte[90];

                    //Console.WriteLine("\n < received length > : " + DispString.Length);
                    //for (int i = 0; i < DispString.Length; i++)
                    //{
                    //    receivedData[i] = (byte)DispString[i];
                    //    Console.Write((int)receivedData[i] + " ");
                    //    //Console.Write(receivedData);
                    //}

                    //totalReceivedLength += DispString.Length;
                    //Console.WriteLine("\n총 데이터 길이 : " + totalReceivedLength);

                    //CloseSerialPort();
                }
                serialPort1.DiscardInBuffer();
            }
            catch
            {

            }
        }



        private void DisplayText(object sender, EventArgs e)
        {
            textBox2.Clear();
            textBox2.AppendText(DispString);
        }

        // Delay
        private static DateTime Delay(int MS)
        {
            DateTime ThisMoment = DateTime.Now;
            TimeSpan duration = new TimeSpan(0, 0, 0, 0, MS);
            DateTime AfterWards = ThisMoment.Add(duration);

            while (AfterWards >= ThisMoment)
            {
                System.Windows.Forms.Application.DoEvents();
                ThisMoment = DateTime.Now;
            }

            return DateTime.Now;
        }



        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream _ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(_ms);
            return returnImage;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MemoryStream ms1 = new MemoryStream();
            String tempName = "C:\\Users\\splee\\Documents\\Images\\test.jpg";
            Image temp1 = Image.FromFile(tempName);

            temp1.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] sendArray1 = new byte[ms1.ToArray().Length];
            sendArray1 = ms1.ToArray();

            pictureBox1.Image = byteArrayToImage(sendArray1);
        }
    }
}
